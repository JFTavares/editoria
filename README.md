# Editoria

Editoria is an online production tool for open publishing.

This application is being developed by the [Coko Foundation](https://coko.foundation/) for the Editoria Community.  
For more information, visit the project's [website](https://editoria.pub/) or our [chat channel](https://mattermost.coko.foundation/coko/channels/editoria).  
The project has been built on top of [Pubsweet](https://gitlab.coko.foundation/pubsweet).
For the editor that Editoria uses, see its related project [Wax](https://gitlab.coko.foundation/wax).

## Roadmap 2020

The following are our goals until the end of the year:

- ~~Move everything to a single repo~~
- ~~Figure out and implement the simplest way to authenticate between services~~
- Separate services (pagedjs & epubcheck) from the app and remove filesystem dependencies in favour of sending files over http
- Dockerize the app
- UI improvements

You can track the above plan in more detail in its [milestone](https://gitlab.coko.foundation/editoria/editoria/-/milestones/12).

We are also in discussions about redesigning parts of the app. You can keep track of that in [these issues](https://gitlab.coko.foundation/editoria/editoria/issues?label_name%5B%5D=Editoria+2020+Review).

## I want to try it

In order to try the application, you should follow the instruction of our [installation](https://gitlab.coko.foundation/editoria/editoria/blob/master/INSTALL.md) document. Also, for any question you could check if there is already an answer in our [FAQ](https://gitlab.coko.foundation/editoria/editoria/blob/master/FAQ.md) document.
